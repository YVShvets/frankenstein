package com.riversoft.test

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories

@SpringBootApplication
@EnableJpaRepositories(basePackages = 'com.riversoft.test.mysql')
@EnableMongoRepositories(basePackages = 'com.riversoft.test.mongo')
@EnableRedisRepositories(basePackages = 'com.riversoft.test.redis')
class FrankensteinApplication {

	static void main(String[] args) {
		SpringApplication.run FrankensteinApplication, args
	}
}
