package com.riversoft.test.controller

import com.riversoft.test.ObjectType
import com.riversoft.test.mongo.domain.Play
import com.riversoft.test.mongo.repository.PlayRepository
import com.riversoft.test.mysql.domain.Lot
import com.riversoft.test.mysql.repository.LotRepository
import com.riversoft.test.redis.domain.Counter
import com.riversoft.test.redis.repository.CounterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by yvshvets on 22.05.16.
 */

@RestController
class TestController {

    @Autowired
    CounterRepository counterRepository

    @Autowired
    PlayRepository playRepository

    @Autowired
    LotRepository lotRepository

    @EventListener
    def init(ContextRefreshedEvent event) {

        counterRepository.deleteAll()
        lotRepository.deleteAll()
        playRepository.deleteAll()
    }

    @RequestMapping('/counters')
    def test() {

        def p = counterRepository.save(new Counter(
                type: ObjectType.values()[new Random().nextInt(2)]
        ))

        counterRepository.findAllByType(ObjectType.TEST.toString()).find()
        //counterRepository.findAll()
    }

    @RequestMapping('/plays')
    def play() {

        def p = playRepository.save(new Play(
                type: ObjectType.values()[new Random().nextInt(2)]
        ))

        playRepository.findByType(ObjectType.TEST).find()
        //playRepository.findAll()
    }

    @RequestMapping('/lots')
    def lots() {

        def p = lotRepository.save(new Lot(
                type: ObjectType.values()[new Random().nextInt(2)]
        ))

        lotRepository.findByType(ObjectType.TEST).find()
        //lotRepository.findAll()
    }
}
