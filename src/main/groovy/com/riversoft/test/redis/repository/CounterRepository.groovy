package com.riversoft.test.redis.repository

import com.riversoft.test.ObjectType
import com.riversoft.test.redis.domain.Counter
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository

/**
 * Created by yvshvets on 22.05.16.
 */
interface CounterRepository extends JpaRepository<Counter, String> {

    List<Counter> findAllByType(String type)
}
