package com.riversoft.test.redis.domain

import com.riversoft.test.ObjectType
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.index.Indexed

import javax.persistence.Column
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

/**
 * Created by yvshvets on 22.05.16.
 */
@RedisHash('counters')
class Counter {

    @Id
    String id

    @Indexed
    @Enumerated(EnumType.STRING)
    ObjectType type
}
