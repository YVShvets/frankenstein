package com.riversoft.test.mongo.repository

import com.riversoft.test.ObjectType
import com.riversoft.test.mongo.domain.Play
import com.riversoft.test.redis.domain.Counter
import org.springframework.data.repository.CrudRepository

interface PlayRepository extends CrudRepository<Play, BigInteger> {

    List<Play> findByType(ObjectType type)
}