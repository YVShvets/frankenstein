package com.riversoft.test.mongo.domain

import com.riversoft.test.ObjectType
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

@Document
class Play {

    @Id
    BigInteger id

    @Enumerated(EnumType.STRING)
    @Indexed
    ObjectType type
}
