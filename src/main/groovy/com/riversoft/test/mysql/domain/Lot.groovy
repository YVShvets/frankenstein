package com.riversoft.test.mysql.domain

import com.riversoft.test.ObjectType

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Table

@Entity
@Table(indexes = @Index(name='lot_type', columnList='type'))
class Lot {

    @Id
    @GeneratedValue
    Long id

    @Enumerated(EnumType.STRING)
    ObjectType type
}
