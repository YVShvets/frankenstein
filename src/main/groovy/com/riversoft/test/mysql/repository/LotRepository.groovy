package com.riversoft.test.mysql.repository

import com.riversoft.test.ObjectType
import com.riversoft.test.mysql.domain.Lot
import com.riversoft.test.redis.domain.Counter
import org.springframework.data.jpa.repository.JpaRepository

interface LotRepository extends JpaRepository<Lot, Long> {

    List<Lot> findByType(ObjectType type)
}
