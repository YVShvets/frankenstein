import org.springframework.util.StopWatch

def times = 0

5000.times {
    def sw = new StopWatch()
    sw.start()
    'http://localhost:8080/lots'.toURL().text
    sw.stop()

    println sw.totalTimeMillis

    if (times != 0) {

        times = (times + sw.totalTimeMillis) / 2
    } else {
        times = sw.totalTimeMillis
    }
}

times
